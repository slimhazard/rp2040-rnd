/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#ifndef RP2040_RND_H
#define RP2040_RND_H

#include <stdint.h>
#include <stdlib.h>

#include "hardware/structs/rosc.h"

static inline uint8_t
rnd_hw(void)
{
	uint8_t rnd = 0;
	for (int i = 0; i < 8; i++)
		rnd |= ((rosc_hw->randombit & 1) << i);
	return (rnd);
}

static inline uint32_t
rnd_hw_mkseed(void)
{
	uint32_t seed = 0;
	for (int i = 0; i < 4; i++) {
		uint8_t byte = rnd_hw() ^ rnd_hw() ^ rnd_hw();
		seed |= byte << (i * 8);
	}
	return (seed);
}

static inline uint32_t
rotl32(const uint32_t n, int bits)
{
	return (n << bits) | (n >> (32 - bits));
}

static inline uint64_t
rotl64(const uint64_t n, int bits)
{
	return (n << bits) | (n >> (64 - bits));
}

static inline uint32_t
rnd32_nxt(uint32_t r[4])
{
	const uint32_t result = r[0] + r[1] + r[3]++;

	r[0] = r[1] ^ (r[1] >> 9);
	r[1] = r[2] ^ (r[2] << 3);
	r[2] = rotl32(r[2], 21) + result;

	return (result);
}

static inline void
rnd32_seed(uint32_t r[4], uint32_t seed[3])
{
	r[0] = seed[0];
	r[1] = seed[1];
	r[2] = seed[2];
	r[3] = 1;
	for (int i = 0; i < 15; i++)
		(void)rnd32_nxt(r);
}

static inline void
rnd32_auto_seed(uint32_t r[4])
{
	uint32_t seed[3];

	seed[0] = rnd_hw_mkseed();
	seed[1] = rnd_hw_mkseed();
	seed[2] = rnd_hw_mkseed();
	rnd32_seed(r, seed);
}

static inline float
rnd32_nxt_flt(uint32_t r[4])
{
	uint32_t r32 = rnd32_nxt(r);
	return ((float)((r32 >> 8) * 0x1.p-24f));
}

static inline uint32_t
rnd32_nxt_bounded(uint32_t r[4], uint32_t bound)
{
        uint32_t mask = UINT32_MAX, result;

        if (bound < (1U << 24))
                return((uint32_t)(rnd32_nxt_flt(r) * bound + 0.5f));

        mask >>= __builtin_clz(bound | 1);
        do {
                result = rnd32_nxt(r) & mask;
        } while (result > bound);
        return (result);
}

static inline uint32_t
rnd32_range(uint32_t r[4], uint32_t min, uint32_t max)
{
	return (min + rnd32_nxt_bounded(r, max - min));
}

static inline uint64_t
rnd64_nxt(uint64_t r[4])
{
	const uint64_t result = r[0] + r[1] + r[3]++;

	r[0] = r[1] ^ (r[1] >> 11);
	r[1] = r[2] ^ (r[2] << 3);
	r[2] = rotl64(r[2], 24) + result;

	return (result);
}

static inline void
rnd64_seed(uint64_t r[4], uint64_t seed[3])
{
	r[0] = seed[0];
	r[1] = seed[1];
	r[2] = seed[2];
	r[3] = 1;
	for (int i = 0; i < 18; i++)
		(void)rnd64_nxt(r);
}

static inline void
rnd64_auto_seed(uint64_t r[4])
{
	uint32_t s32[4];
	uint64_t s64[3];

	rnd32_auto_seed(s32);
	s64[0] = ((uint64_t)rnd32_nxt(s32) << 32) | rnd32_nxt(s32);
	s64[1] = ((uint64_t)rnd32_nxt(s32) << 32) | rnd32_nxt(s32);
	s64[2] = ((uint64_t)rnd32_nxt(s32) << 32) | rnd32_nxt(s32);
	rnd64_seed(r, s64);
}

static inline double
rnd64_nxt_dbl(uint64_t r[4])
{
	uint64_t r64 = rnd64_nxt(r);
	return ((double)((r64 >> 11) * 0x1.p-53d));
}

static inline uint64_t
rnd64_nxt_bounded(uint64_t r[4], uint64_t bound)
{
        uint64_t mask = UINT64_MAX, result;

        if (bound < (1ULL << 53))
                return((uint64_t)(rnd64_nxt_dbl(r) * bound + 0.5d));

        mask >>= __builtin_clz(bound | 1);
        do {
                result = rnd64_nxt(r) & mask;
        } while (result > bound);
        return (result);
}

static inline uint64_t
rnd64_range(uint64_t r[4], uint64_t min, uint64_t max)
{
	return (min + rnd64_nxt_bounded(r, max - min));
}

#endif // #ifndef RP2040_RND_H
