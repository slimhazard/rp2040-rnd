This library is obsolete
------------------------

rp2040-rnd has been obsolete since the
[pico_rand](https://www.raspberrypi.com/documentation/pico-sdk/high_level.html#pico_rand "SDK pico_rand lib")
library was introduced in the
[Pico SDK version 1.5.0](https://github.com/raspberrypi/pico-sdk/releases/tag/1.5.0).
Use pico_rand instead.
