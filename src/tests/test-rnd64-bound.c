/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include <stdlib.h>

#include "pico/stdlib.h"
#include "pico/stdio.h"
#include "hardware/structs/systick.h"
#include "tusb.h"
#include "ticks.h"

#include <rnd.h>

#define SAMPLES (1000)

#define const_bound(d)							\
	do {								\
		printf("\nbound_const_%llu,ns\n", d);			\
		for (int i = 0; i < SAMPLES; i++) {			\
			systick_hw->cvr = 0;				\
			r64 = rnd64_nxt_bounded(s64, d);		\
			ticks = systick_hw->cvr;			\
			printf("%llu,%lu\n", r64, ticks2ns(ticks));	\
		}							\
	} while(0)

int
main(void)
{
	volatile uint32_t ticks;
	uint64_t r64, s64[4];

	stdio_init_all();

	while (!tud_cdc_connected())
		sleep_ms(100);

	rnd64_auto_seed(s64);

	systick_hw->csr = 5;
	systick_hw->rvr = 0xffffff;

	const_bound(10ULL);
	const_bound(100ULL);
	const_bound(1000ULL);
	const_bound(10000ULL);
	const_bound(100000ULL);
	const_bound(1000000ULL);
	const_bound(10000000ULL);
	const_bound(100000000ULL);
	const_bound(1000000000ULL);
	const_bound(10000000000ULL);
	const_bound(100000000000ULL);
	const_bound(1000000000000ULL);
	const_bound(10000000000000ULL);
	const_bound(100000000000000ULL);
	const_bound(1000000000000000ULL);
	const_bound(10000000000000000ULL);
	const_bound(100000000000000000ULL);
	const_bound(1000000000000000000ULL);
	const_bound(10000000000000000000ULL);

	uint64_t d = 10;
	for (int p = 0; p < 19; p++, d *= 10) {
		printf("\nbound_%llu,ns\n", d);
		for (int i = 0; i < SAMPLES; i++) {
			systick_hw->cvr = 0;
			r64 = rnd64_nxt_bounded(s64, d);
			ticks = systick_hw->cvr;
			printf("%llu,%lu\n", r64, ticks2ns(ticks));
		}
	}

	for (;;)
		__wfi();
}
