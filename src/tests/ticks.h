/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>

static inline uint32_t
tick_cnt(uint32_t ticks)
{
	return (0x0ffffff - ticks);
}

static inline uint32_t
ticks2ns(uint32_t ticks)
{
	return (tick_cnt(ticks) * 8);
}
