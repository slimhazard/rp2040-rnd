/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include <stdlib.h>

#include "pico/stdlib.h"
#include "pico/stdio.h"
#include "hardware/structs/systick.h"
#include "tusb.h"
#include "ticks.h"

#include <rnd.h>

#define SAMPLES (1000)

#if 0
static inline uint32_t
rnd_hw_ent(void)
{
	uint32_t result = 0;
	for (int i = 0; i < 4; i++) {
		uint8_t byte = 0;
		for (int j = 0; j < 8; j++)
			byte |= ((rosc_hw->randombit & 1) << j);
		for (int j = 0; j < 8; j++)
			byte ^= ((rosc_hw->randombit & 1) << j);
		result |= byte << (i * 8);
	}
	return (result);
}
#endif

int
main(void)
{
	uint8_t r8;
	uint32_t r32, s32[4];
	uint64_t r64, s64[4];
	volatile uint32_t ticks;

	stdio_init_all();

	while (!tud_cdc_connected())
		sleep_ms(100);

	systick_hw->csr = 5;
	systick_hw->rvr = 0xffffff;

	puts("\nrnd_hw,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		r8 = rnd_hw();
		ticks = systick_hw->cvr;
		printf("%u,%lu\n", r8, ticks2ns(ticks));
	}

	puts("\nrnd_hw_mkseed,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		r32 = rnd_hw_mkseed();
		ticks = systick_hw->cvr;
		printf("%lu,%lu\n", r32, ticks2ns(ticks));
	}

	puts("\nstdlib_srand_ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		srand(r32);
		ticks = systick_hw->cvr;
		printf("%lu\n", ticks2ns(ticks));
	}

	puts("\nrnd32_auto_seed,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		rnd32_auto_seed(s32);
		ticks = systick_hw->cvr;
		r32 = rnd32_nxt(s32);
		printf("%lu,%lu\n", r32, ticks2ns(ticks));
	}

	puts("\nrnd64_auto_seed,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		rnd64_auto_seed(s64);
		ticks = systick_hw->cvr;
		r64 = rnd64_nxt(s64);
		printf("%llu,%lu\n", r64, ticks2ns(ticks));
	}

	for (;;)
		__wfi();
}
