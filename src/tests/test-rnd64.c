/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "pico/stdlib.h"
#include "pico/stdio.h"
#include "hardware/structs/systick.h"
#include "tusb.h"
#include "ticks.h"

#include <rnd.h>

#define SAMPLES (1000)

int
main(void)
{
	volatile uint32_t ticks;
	uint64_t r64, s64[4];
	double d;

	stdio_init_all();

	while (!tud_cdc_connected())
		sleep_ms(100);

	rnd64_auto_seed(s64);

	systick_hw->csr = 5;
	systick_hw->rvr = 0xffffff;

	puts("\nrnd64_nxt,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		r64 = rnd64_nxt(s64);
		ticks = systick_hw->cvr;
		printf("%llu,%lu\n", r64, ticks2ns(ticks));
	}

	puts("\nrnd_nxt_dbl,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		d = rnd64_nxt_dbl(s64);
		ticks = systick_hw->cvr;
		printf("%.*g,%lu\n", DBL_DIG, d, ticks2ns(ticks));
	}

	for (;;)
		__wfi();
}
