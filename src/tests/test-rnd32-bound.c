/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include <stdlib.h>

#include "pico/stdlib.h"
#include "pico/stdio.h"
#include "hardware/structs/systick.h"
#include "tusb.h"
#include "ticks.h"

#include <rnd.h>

#define SAMPLES (1000)

#define const_bound(d)							\
	do {								\
		printf("\nbound_const_%d,ns\n", d);			\
		for (int i = 0; i < SAMPLES; i++) {			\
			systick_hw->cvr = 0;				\
			r32 = rnd32_nxt_bounded(s32, d);		\
			ticks = systick_hw->cvr;			\
			printf("%lu,%lu\n", r32, ticks2ns(ticks));	\
		}							\
	} while(0)

int
main(void)
{
	volatile uint32_t ticks;
	uint32_t r32, s32[4];

	stdio_init_all();

	while (!tud_cdc_connected())
		sleep_ms(100);

	rnd32_auto_seed(s32);

	systick_hw->csr = 5;
	systick_hw->rvr = 0xffffff;

	const_bound(10);
	const_bound(100);
	const_bound(1000);
	const_bound(10000);
	const_bound(100000);
	const_bound(1000000);
	const_bound(10000000);
	const_bound(100000000);
	const_bound(1000000000);

	for (int d = 10; d <= 1000000000; d *= 10) {
		printf("\nbound_%d,ns\n", d);
		for (int i = 0; i < SAMPLES; i++) {
			systick_hw->cvr = 0;
			r32 = rnd32_nxt_bounded(s32, d);
			ticks = systick_hw->cvr;
			printf("%lu,%lu\n", r32, ticks2ns(ticks));
		}
	}

	for (;;)
		__wfi();
}
