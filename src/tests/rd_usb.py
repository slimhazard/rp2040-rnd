#!/usr/bin/env python3

import usb.core
import usb.backend.libusb1
import sys

# These MUST match the VID, PID and IN endpoint number in usb_descriptors.c.
VENDOR_ID = 0x2e8a
PRODUCT_ID = 0xb1d5
EP_IN = 0x81

size = 64
timeout = None

if len(sys.argv) > 3:
    print('Usage: ' + sys.argv[0] + ' [bytes] [timeout_ms]', file=sys.stderr)
    sys.exit(-1)

if len(sys.argv) > 2:
    timeout = int(sys.argv[2])
    if timeout <= 0:
        timeout = None

if len(sys.argv) > 1:
    size_arg = int(sys.argv[1])
    if size_arg < 0:
        print('Size in bytes must be >= 0', file=sys.stderr)
        sys.exit(-1)
    size = int((size_arg + 63) / 64) * int(64)
    if size != size_arg:
        print('NOTE: size rounded up to ' + str(size), file=sys.stderr)

device = usb.core.find(idVendor=VENDOR_ID, idProduct=PRODUCT_ID)
if device == None:
    print('Device with vendor id ' + hex(VENDOR_ID) + ' and product id ' +
          hex(PRODUCT_ID) + ' not found', file=sys.stderr)
    sys.exit(-1)

usb.util.claim_interface(device, 0)

if size == 0:
    while True:
        data = device.read(EP_IN, 64, None)
        sys.stdout.buffer.write(data)

data = device.read(EP_IN, size, timeout)
if len(data) < size:
    size = size - len(data)
    while True:
        tmp =  device.read(EP_IN, size, timeout)
        data += tmp
        size = size - len(tmp)
        if size == 0:
            break

sys.stdout.buffer.write(data)
