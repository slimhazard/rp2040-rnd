/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "pico/stdlib.h"
#include "pico/stdio.h"
#include "hardware/structs/systick.h"
#include "tusb.h"
#include "ticks.h"

#include <rnd.h>

#define SAMPLES (1000)

int
main(void)
{
	uint32_t r32, s32[4];
	volatile uint32_t ticks;
	float f;
	int stdlib_r;

	stdio_init_all();

	   while (!tud_cdc_connected())
		sleep_ms(100);

	srand(rnd_hw_mkseed());
	rnd32_auto_seed(s32);

	systick_hw->csr = 5;
	systick_hw->rvr = 0xffffff;

	puts("\nstdlib_rand,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		stdlib_r = rand();
		ticks = systick_hw->cvr;
		printf("%lu,%lu\n", stdlib_r, ticks2ns(ticks));
	}

	puts("\nrnd32_nxt,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		r32 = rnd32_nxt(s32);
		ticks = systick_hw->cvr;
		printf("%lu,%lu\n", r32, ticks2ns(ticks));
	}

	puts("\nrnd_nxt_flt,ns");
	for (int i = 0; i < SAMPLES; i++) {
		systick_hw->cvr = 0;
		f = rnd32_nxt_flt(s32);
		ticks = systick_hw->cvr;
		printf("%.*f,%lu\n", FLT_DIG, f, ticks2ns(ticks));
	}

	for (;;)
		__wfi();
}
