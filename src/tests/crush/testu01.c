/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>

#include "TestU01.h"
#include "libusb-1.0/libusb.h"

#define VID   (0x2e8a)
#define PID   (0xb1d5)
#define EP_IN (0x81)
#define INTF  (0)

#define RECS_SCALE (6)
#define NRECS      (1U << RECS_SCALE)
#define RECS_MASK  (NRECS - 1)

#define REC_SCALE (8)
#define REC_WORDS (1U << REC_SCALE)
#define REC_BYTES (REC_WORDS * sizeof(uint32_t))

static uint32_t buf[NRECS][REC_WORDS];
static sem_t empty, full;

static inline void
fail(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	exit(EXIT_FAILURE);
}

static void *
usb_thread(void *arg)
{
	libusb_device_handle *dev_hndl = arg;
	static int idx = 0;
	unsigned char *p;
	int ret, xfer;

	for (;;) {
		errno = 0;
		if ((ret = sem_wait(&empty)) != 0)
			fail("sem_wait(empty): %s\n", strerror(errno));
		if ((ret = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL))
		    != 0)
			fail("pthread_setcancelstate(DISABLE): %s\n",
			     strerror(errno));

		p = (unsigned char *)buf[idx];
		for (int n = REC_BYTES; n > 0; p += xfer) {
			ret = libusb_bulk_transfer(dev_hndl, EP_IN, p, n, &xfer,
						   0);
			if (ret != 0)
				fail("libusb_bulk_transfer(): %d (%s)\n",
				     ret, libusb_error_name(ret));
			if (xfer > n)
				fail("xfer > n");
			n -= xfer;
		}

		errno = 0;
		if ((ret = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL))
		    != 0)
			fail("pthread_setcancelstate(ENABLE): %s\n",
			     strerror(errno));
		if ((ret = sem_post(&full)) != 0)
			fail("sem_post(full): %s\n", strerror(errno));
		idx++;
		idx &= RECS_MASK;
	}
	/* Unreachable */
	return NULL;
}

static unsigned int
getRnd(void)
{
	static uint32_t local_buf[REC_WORDS];
	static int local_idx = REC_WORDS;
	static int buf_idx = 0;
	int ret;

	if (local_idx == REC_WORDS) {
		errno = 0;
		if ((ret = sem_wait(&full)) != 0)
			fail("sem_wait(full): %s\n", strerror(errno));

		memcpy(local_buf, buf[buf_idx], REC_BYTES);

		errno = 0;
		if ((ret = sem_post(&empty)) != 0)
			fail("sem_post(empty): %s\n", strerror(errno));
		local_idx = 0;
		buf_idx++;
		buf_idx &= RECS_MASK;
	}
	return local_buf[local_idx++];
}

int
main(void)
{
	libusb_device_handle *dev_hndl;
	unif01_Gen* gen;
	pthread_t usb_rdr;
	int ret;
	void *cancel;

	ret = libusb_init(NULL);
	if (ret != 0)
		fail("libusb_init(): %d (%s)\n", ret, libusb_error_name(ret));

	dev_hndl = libusb_open_device_with_vid_pid(NULL, VID, PID);
	if (dev_hndl == NULL)
		fail("Cannot open device with VID=%04x PID=%04x\n", VID, PID);
	ret = libusb_claim_interface(dev_hndl, INTF);
	if (ret != 0)
		fail("libusb_claim_interface(): %d (%s)\n", ret,
		     libusb_error_name(ret));

	errno = 0;
	if ((ret = sem_init(&empty, 0, NRECS)) != 0)
		fail("sem_init(empty): %s\n", strerror(errno));
	if ((ret = sem_init(&full, 0, 0)) != 0)
		fail("sem_init(full): %s\n", strerror(errno));
	if ((ret = pthread_create(&usb_rdr, NULL, &usb_thread, dev_hndl)) != 0)
		fail("pthread_create(): %s\n", strerror(errno));

	gen = unif01_CreateExternGenBits("rp2040-rnd", getRnd);

	bbattery_SmallCrush(gen);

	unif01_DeleteExternGenBits(gen);

	if ((ret = pthread_cancel(usb_rdr)) != 0)
		fail("pthread_cancel(): %s\n", strerror(errno));
	if ((ret = pthread_join(usb_rdr, &cancel)) != 0)
		fail("pthread_cancel(): %s\n", strerror(errno));
	if (cancel != PTHREAD_CANCELED)
		fail("Thread not canceled\n");

	ret = libusb_release_interface(dev_hndl, INTF);
	if (ret != 0)
		fail("libusb_release_interface(): %d (%s)\n", ret,
		     libusb_error_name(ret));
	libusb_close(dev_hndl);
	libusb_exit(NULL);

	if ((ret = sem_destroy(&empty)) != 0)
		fail("sem_destroy(empty): %s\n", strerror(errno));
	if ((ret = sem_destroy(&full)) != 0)
		fail("sem_destroy(full): %s\n", strerror(errno));

	return EXIT_SUCCESS;
}
