/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>

#include <rnd.h>

#include "pico/multicore.h"
#include "pico/sem.h"
#include "pico/stdlib.h"
#include "hardware/clocks.h"

#include "bsp/board.h"
#include "tusb.h"

#define BUF_SCALE (3)
#define NBUFS     (1U << BUF_SCALE)
#define BUF_MASK  (NBUFS - 1)

#define REC_SCALE (12)
#define REC_WORDS (1U << REC_SCALE)
#define REC_BYTES (REC_WORDS * sizeof(uint32_t))

static uint32_t buf[NBUFS][REC_WORDS];

static semaphore_t empty, full;

#ifndef NDEBUG
#include <stdio.h>
#include <stdarg.h>

#include "pico/stdio_uart.h"

static inline void
debug(const char *fmt, ...)
{
        va_list args;
        va_start(args, fmt);
        vprintf(fmt, args);
        va_end(args);
}
#else
static inline void
debug(const char *fmt, ...)
{
        (void)fmt;
}
#endif

void
core1_main(void)
{
	uint32_t r[4];
	int idx = 0;

	rnd32_auto_seed(r);

	for (;;) {
		sem_acquire_blocking(&empty);
		for (uint32_t *p = buf[idx], *e = p + REC_WORDS; p < e; p++)
			*p = rnd32_nxt(r);
		idx++;
		idx &= BUF_MASK;
		sem_release(&full);
	}
}

int
main(void)
{
	int idx = 0;
	unsigned buf_avlbl = 0;
	uint8_t *p;

#ifndef NDEBUG
        stdout_uart_init();
        uart_set_baudrate(PICO_DEFAULT_UART_INSTANCE, 921600);
#endif

	/* Values from vcocalc.py to set clk_sys to 133 MHz */
	set_sys_clock_pll(1596 * MHZ, 6, 2);

	sem_init(&empty, NBUFS, NBUFS);
	sem_init(&full, 0, NBUFS);

	multicore_launch_core1(core1_main);

	board_init();
	tusb_init();

	for (;;) {
		uint32_t avlbl;

		tud_task();

		if (!tud_vendor_mounted())
			goto wait;

		if (tud_vendor_available())
			tud_vendor_read_flush();

		if ((avlbl = tud_vendor_write_available()) == 0)
			goto wait;

		while (avlbl > 0) {
			uint32_t written, n = avlbl;

			if (buf_avlbl == 0) {
				sem_acquire_blocking(&full);
				p = (uint8_t *)buf[idx];
				buf_avlbl = REC_BYTES;
			}

			if (n > buf_avlbl)
				n = buf_avlbl;
			written = tud_vendor_write(p, n);
			buf_avlbl -= written;
			avlbl -= written;
			p += written;

			if (buf_avlbl == 0) {
				sem_release(&empty);
				idx++;
				idx &= BUF_MASK;
			}
		}

	wait:
		while (!tud_task_event_ready())
			__wfe();
	}
}
